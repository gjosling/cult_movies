# Clean titles ------------------------------------------------------------
dt_films_imdb <- dt_movies[dt_links, on = "movieId"
                           ][dt_imdb, on = c("imdbId" = "imdb_title_id"), nomatch = 0]

dt_films_imdb <- dt_films_imdb[,
                                 title := gsub(" \\(.+", "", title, perl = TRUE)]

dt_films_imdb[title %like% ", The$",
               title := paste0("The ", gsub(", The", "", title))]
dt_films_imdb[title %like% ", A$",
               title := paste0("A ", gsub(", A$", "", title))]

# Combine wiki list and IMDB ----------------------------------------------
# Find those with matching titles
dt_films_cult <- merge(dt_films_wiki[, .(title = title, title_wiki_ori = Film, year_wiki = Year)],
                     dt_films_imdb[, .(title = title, year_imdb = year, imdbId)],
                     by = c("title"), all.x = TRUE)
# Years must be within one year
dt_films_cult <- dt_films_cult[!is.na(imdbId) & abs(year_imdb - year_wiki) > 1,
                               imdbId := NA]
dt_films_cult[, uniqueN(imdbId) / .N] # Most have a match

# Remove those with multiple possible matches
delete_id <- dt_films_cult[!is.na(imdbId),
                           .N,
                           .(title, year_wiki)
                           ][N > 1, paste0(title, year_wiki)]
dt_films_cult <- dt_films_cult[!paste0(title, year_wiki) %in% delete_id]

# Which don't have a match?
dt_films_unmatched <- dt_films_cult[,
                                    max(!is.na(imdbId)),
                                    .(title, title_wiki_ori, year_wiki)
                                    ][V1 == 0]

# Fuzzy match
dt_films_unmatched_fuzzy <- setDT(
  stringdist_left_join(
    dt_films_unmatched,
    dt_films_imdb[, .(title, year_imdb = year, imdbId)],
    by = "title",
    distance_col = "dist"))

# Films that fuzzy join on title and are within a year of each other
dt_films_possible_match <- dt_films_unmatched_fuzzy[!is.na(imdbId) &
                                                      abs(year_imdb - year_wiki) <= 1]

# Remove the ones with multiple matches
delete_id <- dt_films_possible_match[, .N, title.x][N > 1, title.x]
dt_films_possible_match <- dt_films_possible_match[!title.x %in% delete_id]

# Remove the ones with short titles
delete_id <- dt_films_possible_match[nchar(title.x) <= 5 |
                                 (nchar(title.x) <= 8 & dist > 1),
                               title.x]
dt_films_possible_match <- dt_films_possible_match[!title.x %in% delete_id]
dt_films_possible_match[, .N] # 87 more matches

# Try again with other imdb title column
dt_films_unmatched_fuzzy2 <- setDT(
  stringdist_left_join(
    dt_films_unmatched[title %in% dt_films_possible_match$title.x],
    dt_films_imdb[title != i.title,
                  .(title = i.title,
                    year_imdb = year,
                    imdbId)],
    by = "title",
    distance_col = "dist"))

dt_films_possible_match2 <- dt_films_unmatched_fuzzy2[!is.na(imdbId) &
                                                        abs(year_imdb - year_wiki) <= 1]

cult_movies <- unique(c(dt_films_cult[!is.na(imdbId), imdbId],
                        dt_films_possible_match$imdbId,
                        dt_films_possible_match2$imdbId))

length(cult_movies) / dt_films_wiki[, .N]

dt_films_imdb[, is_cult := imdbId %in% cult_movies] # > 80% match - ok
